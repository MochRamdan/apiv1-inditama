<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Resources\ProductResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produts = Product::latest()->paginate(5);
        return new ProductResource(true, 'List Products', $produts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_product_id' => 'required',
            'name' => 'required|max:100',
            'price' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $image = $request->file('image');
        $image->storeAs('public/products', $image->getClientOriginalName());

        $product = Product::create([
            'category_product_id' => $request->category_product_id,
            'name' => $request->name,
            'price' => $request->price,
            'image' => $image->getClientOriginalName()
        ]);

        return new ProductResource(true, 'product has been added', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return new ProductResource(true, 'product find', $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // $validator = Validator::make($request->all(), [
        //     'category_product_id' => 'required',
        //     'name' => 'required|max:100',
        //     'price' => 'required',
        //     'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg'
        // ]);

        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), 422);
        // }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image->storeAs('public/products', $image->getClientOriginalName());
            Storage::delete('public/products/'.$product->image);
            $product->update([
                'category_product_id' => $request->category_product_id,
                'name' => $request->name,
                'price' => $request->price,
                'image' => $image->getClientOriginalName()
            ]);

        } else {
            $product->update([
                'category_product_id' => $request->category_product_id,
                'name' => $request->name,
                'price' => $request->price,
            ]);
        }
        return new ProductResource(true, 'Product updated', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        Storage::delete('public/products/'.$product->image);
        $product->delete();
        return new ProductResource(true, 'product has been deleted', $product);
    }
}
