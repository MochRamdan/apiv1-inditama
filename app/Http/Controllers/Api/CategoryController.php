<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryProduct;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\CategoryProductResource;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = CategoryProduct::latest()->paginate(5);
        return new CategoryProductResource(true, 'List Categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'image' => 'image|mimes:jpg,png,jpeg,gif,svg'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $image = $request->file('image');
        $image->storeAs('public/categories', $image->getClientOriginalName());

        $category = CategoryProduct::create([
            'name' => $request->name,
            'image' => $image->getClientOriginalName()
        ]);

        return new CategoryProductResource(true, 'category has been added', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = CategoryProduct::find($id);
        return new CategoryProductResource(true, 'category find', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = CategoryProduct::find($id);

        if($category){
            if ($request->hasFile('image')) {
                $image = $request->file('image');
                $image->storeAs('public/categories', $image->getClientOriginalName());
    
                Storage::delete('public/categories/'.$category->image);
    
                $category->update([
                    'name' => $request->name,
                    'image' => $image->getClientOriginalName()
                ]);
    
            } else {
                $category->update([
                    'name' => $request->name
                ]);
            }
        }

        return new CategoryProductResource(true, 'category updated', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
