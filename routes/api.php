<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\LogoutController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', RegisterController::class)->name('register');

Route::post('/login', LoginController::class)->name('login');

Route::post('/logout', LogoutController::class)->name('logout');

Route::middleware('auth:api')->group(function () {
    Route::get('/users', function (Request $request) {
        return $request->user();
    });

    Route::resource('category-product', CategoryController::class)->except(['create', 'destroy', 'edit'])->names([
        'index' => 'category.index',
        'show' => 'category.show',
        'store' => 'category.store',
        'update' => 'category.update'
    ]);

    Route::resource('product', ProductController::class)->except(['create', 'edit'])->names([
        'index' => 'product.index',
        'show' => 'product.show',
        'store' => 'product.store',
        'update' => 'product.update'
    ]);
});
